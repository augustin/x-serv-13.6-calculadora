def sumar (a: int, b: int):
    """
    Esta funcion suma dos numeros enteros
    devuelve la opreacion y el resultado en c
    """

    c: int = a + b
    print(str(a) + " + " + str(b) + " = " + str(c))

def restar (a: int, b: int):
    """
    Esta funcion resta dos numeros enteros
    devuelve la operacion y el resultado en c
    """

    c: int = a - b
    print(str(a) + " - " + str(b) + " = " + str(c))

sumar(1, 2)
sumar(3, 4)
restar(6, 5)
restar(8, 7)
